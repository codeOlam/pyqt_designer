# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Login_1.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form_login(object):
    def setupUi(self, Form_login):
        Form_login.setObjectName("Form_login")
        Form_login.resize(523, 376)
        Form_login.setMinimumSize(QtCore.QSize(523, 376))
        Form_login.setMaximumSize(QtCore.QSize(523, 376))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/powerIcon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form_login.setWindowIcon(icon)
        self.label_login = QtWidgets.QLabel(Form_login)
        self.label_login.setGeometry(QtCore.QRect(180, 50, 181, 71))
        font = QtGui.QFont()
        font.setPointSize(35)
        font.setBold(True)
        font.setWeight(75)
        self.label_login.setFont(font)
        self.label_login.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_login.setAlignment(QtCore.Qt.AlignCenter)
        self.label_login.setObjectName("label_login")
        self.label_uname = QtWidgets.QLabel(Form_login)
        self.label_uname.setGeometry(QtCore.QRect(114, 150, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_uname.setFont(font)
        self.label_uname.setObjectName("label_uname")
        self.label_pswd = QtWidgets.QLabel(Form_login)
        self.label_pswd.setGeometry(QtCore.QRect(114, 200, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_pswd.setFont(font)
        self.label_pswd.setObjectName("label_pswd")
        self.lineEdit_uname = QtWidgets.QLineEdit(Form_login)
        self.lineEdit_uname.setGeometry(QtCore.QRect(212, 150, 131, 36))
        self.lineEdit_uname.setObjectName("lineEdit_uname")
        self.lineEdit_pswd = QtWidgets.QLineEdit(Form_login)
        self.lineEdit_pswd.setGeometry(QtCore.QRect(212, 200, 131, 36))
        self.lineEdit_pswd.setObjectName("lineEdit_pswd")
        self.pushButton_login = QtWidgets.QPushButton(Form_login)
        self.pushButton_login.setGeometry(QtCore.QRect(170, 260, 92, 36))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/vector-login.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_login.setIcon(icon1)
        self.pushButton_login.setObjectName("pushButton_login")
        self.pushButton_cancel = QtWidgets.QPushButton(Form_login)
        self.pushButton_cancel.setGeometry(QtCore.QRect(280, 260, 92, 36))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/cancelIcon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_cancel.setIcon(icon2)
        self.pushButton_cancel.setObjectName("pushButton_cancel")

        self.retranslateUi(Form_login)
        QtCore.QMetaObject.connectSlotsByName(Form_login)

    def retranslateUi(self, Form_login):
        _translate = QtCore.QCoreApplication.translate
        Form_login.setWindowTitle(_translate("Form_login", "Login Form"))
        self.label_login.setText(_translate("Form_login", "Login"))
        self.label_uname.setText(_translate("Form_login", "Username"))
        self.label_pswd.setText(_translate("Form_login", "Password"))
        self.pushButton_login.setText(_translate("Form_login", "Login"))
        self.pushButton_cancel.setText(_translate("Form_login", "cancel"))




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form_login = QtWidgets.QWidget()
    ui = Ui_Form_login()
    ui.setupUi(Form_login)
    Form_login.show()
    sys.exit(app.exec_())

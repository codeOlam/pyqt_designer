from PyQt5 import QtWidgets
from home import Ui_MainWindow_home
from login import Ui_Form_login
from stock_entry import Ui_StockEntry


class InventoryApp(QtWidgets.QWidget, Ui_Form_login):
	def __init__(self):
		super().__init__()

		self.setupUi(self)

		
		self.initializations()

	def initializations(self):
		#Login with details to Open Home Window
		self.pushButton_login.clicked.connect(self.homeWindow)


		self.show()

	def loginHandler(self):
		pass

	def homeWindow(self):
		self.MainWindow_home = QtWidgets.QMainWindow()
		self.ui = Ui_MainWindow_home()
		self.ui.setupUi(self.MainWindow_home)
		self.MainWindow_home.show()
		#hide login window
		InventoryApp.hide(self)



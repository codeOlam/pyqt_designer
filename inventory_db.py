import sys
from PyQt5.QtWidgets import QApplication, QMessageBox
import MySQLdb as mdb 

def connectDB(self):
	try:
		db = mdb.connect('localhost', 'USERNAME', 'PASSWORD', 'db_name')
		QMessageBox.about(self, 'db_connection', 'Connection is Successfull')

	except mdb.Error as e:
		QMessageBox.about(self, 'db_connection', 'Connection Failed')
		sys.exit(1)



def initialization(self):

	self.n_lineEdit = QLineEdit(self)
	self.e_lineEdit = QLineEdit(self)
	self.no_lineEdit = QLineEdit(self)

	self.button_insert = QPushbutton("Insert Data", self)
	self.button_insert.clicked.connect(self.insert_data)


def insert_data(self):
	con = mdb.connect('localhost', 'USERNAME', 'PASSWORD', 'db_name')

	with con:
		cur = con.cursor()
		cur.execute("INSERT INTO data(name, email, phone)"
					"VALUES('%s', '%s', '%s')" %(''.join(self.n_lineEdit.text()),
											''.join(self.e_lineEdit.text()),
											''.join(self.no_lineEdit.text())
											))

		QMessageBox.about(self, 'db_connection', 'Data inserted Successfully')
		self.close()


#when working with qt-desinger. this is done from a table
def insertData(self):
	name = [self.<.py_filename>.item(row, 0).text() for row in range(self.<.py_filename>.rowCount())]
	email = [self.<.py_filename>.item(row, 1).text() for row in range(self.<.py_filename>.rowCount())]
	phone = [self.<.py_filename>.item(row, 2).text() for row in range(self.<.py_filename>.rowCount())]


	con = mdb.connect('localhost', 'USERNAME', 'PASSWORD', 'db_name')

	with con:
		cur = con.cursor()
		cur.execute("INSERT INTO data(name, email, phone)"
					"VALUES('%s', '%s', '%s')" %(''.join(name),
											''.join(email),
											''.join(phone)
											))

		QMessageBox.about(self, 'db_connection', 'Data inserted Successfully')
		self.close()

#then give the pushButton click event


# to retrive data from data base with gt-Desiner
def LoadData(self):
	con = mdb.connect('localhost', 'USERNAME', 'PASSWORD', 'db_name')

	with con:
		cur = con.cursor()
		cur.execute("SELECT * FROM data")

		for i in range(cur.rowCount)
		result = cur.fetchall()

		for row in result:
			self.cursor = QTextCursor(self.textEdit.document())
			self.cursor.insertText(str(row))


#Witing update function 
def update_db(self):
	con = mdb.connector.connect(
	  host="localhost",
	  user="yourusername",
	  passwd="yourpassword",
	  database="mydatabase"
	)

	cur = con.cursor()

	new_data = "UPDATE data SET address = 'Canyon 123' WHERE address = 'Valley 345'"

	cur.execute(new_data)

	con.commit()


#selecting perticular columns

mydb = mysql.connector.connect(
  host="localhost",
  user="yourusername",
  passwd="yourpassword",
  database="mydatabase"
)

mycursor = mydb.cursor()

mycursor.execute("SELECT name, address FROM customers")

myresult = mycursor.fetchall()

################################################################################
#																			   #
#Writing the Logic that will help to automatically populate the table in the   #
#inventory detail view                                                         #
################################################################################


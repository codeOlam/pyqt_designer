# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Home.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from stock_entry import Ui_StockEntry
from stock_detail import Ui_StockDetails


class Ui_MainWindow_home(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

    def setupUi(self, MainWindow_home):
        MainWindow_home.setObjectName("MainWindow_home")
        MainWindow_home.resize(800, 600)
        MainWindow_home.setMinimumSize(QtCore.QSize(800, 600))
        MainWindow_home.setMaximumSize(QtCore.QSize(800, 600))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/powerIcon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow_home.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow_home)
        self.centralwidget.setObjectName("centralwidget")
        self.label_banner = QtWidgets.QLabel(self.centralwidget)
        self.label_banner.setGeometry(QtCore.QRect(20, 40, 761, 231))
        self.label_banner.setText("")
        self.label_banner.setPixmap(QtGui.QPixmap("images/masteringthe.jpg"))
        self.label_banner.setScaledContents(True)
        self.label_banner.setObjectName("label_banner")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(219, 289, 411, 221))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame.setLineWidth(4)
        self.frame.setObjectName("frame")
        self.pushButton_stockEntry = QtWidgets.QPushButton(self.frame)
        self.pushButton_stockEntry.setGeometry(QtCore.QRect(30, 30, 151, 61))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/stock_entry.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_stockEntry.setIcon(icon1)
        self.pushButton_stockEntry.setIconSize(QtCore.QSize(50, 50))
        self.pushButton_stockEntry.setObjectName("pushButton_stockEntry")
        self.pushButton_stockDetail = QtWidgets.QPushButton(self.frame)
        self.pushButton_stockDetail.setGeometry(QtCore.QRect(210, 30, 151, 61))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/view_stock.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_stockDetail.setIcon(icon2)
        self.pushButton_stockDetail.setIconSize(QtCore.QSize(50, 50))
        self.pushButton_stockDetail.setObjectName("pushButton_stockDetail")
        self.pushButton_Equipments = QtWidgets.QPushButton(self.frame)
        self.pushButton_Equipments.setGeometry(QtCore.QRect(30, 110, 151, 61))
        self.pushButton_Equipments.setIcon(icon1)
        self.pushButton_Equipments.setIconSize(QtCore.QSize(50, 50))
        self.pushButton_Equipments.setObjectName("pushButton_Equipments")


        self.pushButton_report = QtWidgets.QPushButton(self.frame)
        self.pushButton_report.setGeometry(QtCore.QRect(210, 110, 151, 61))
        self.pushButton_report.setIcon(icon1)
        self.pushButton_report.setIconSize(QtCore.QSize(50, 50))
        self.pushButton_report.setObjectName("pushButton_report")
        self.pushButton_report.clicked.connect(self.report)
        
        MainWindow_home.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow_home)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 28))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuInventory = QtWidgets.QMenu(self.menubar)
        self.menuInventory.setObjectName("menuInventory")
        self.menuRegions = QtWidgets.QMenu(self.menubar)
        self.menuRegions.setObjectName("menuRegions")
        self.menuStations = QtWidgets.QMenu(self.menuRegions)
        self.menuStations.setObjectName("menuStations")
        self.menuSubStation = QtWidgets.QMenu(self.menuStations)
        self.menuSubStation.setObjectName("menuSubStation")
        MainWindow_home.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow_home)
        self.statusbar.setObjectName("statusbar")
        MainWindow_home.setStatusBar(self.statusbar)
        self.actionExit = QtWidgets.QAction(MainWindow_home)
        self.actionExit.setObjectName("actionExit")
        self.actionEquipments = QtWidgets.QAction(MainWindow_home)
        self.actionEquipments.setObjectName("actionEquipments")
        self.menuFile.addAction(self.actionExit)
        self.menuSubStation.addAction(self.actionEquipments)
        self.menuStations.addAction(self.menuSubStation.menuAction())
        self.menuRegions.addAction(self.menuStations.menuAction())
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuInventory.menuAction())
        self.menubar.addAction(self.menuRegions.menuAction())

        self.retranslateUi(MainWindow_home)
        QtCore.QMetaObject.connectSlotsByName(MainWindow_home)

    def retranslateUi(self, MainWindow_home):
        _translate = QtCore.QCoreApplication.translate
        MainWindow_home.setWindowTitle(_translate("MainWindow_home", "Home"))

        self.pushButton_stockEntry.setText(_translate("MainWindow_home", "Stock Entry"))
        self.pushButton_stockEntry.clicked.connect(self.stock_entry)

        self.pushButton_stockDetail.setText(_translate("MainWindow_home", "Stock Detail"))
        self.pushButton_stockDetail.clicked.connect(self.stock_detail)

        self.pushButton_Equipments.setText(_translate("MainWindow_home", "Equipments"))
        self.pushButton_Equipments.clicked.connect(self.equipments)

        self.pushButton_report.setText(_translate("MainWindow_home", "Report"))
        self.menuFile.setTitle(_translate("MainWindow_home", "File"))
        self.menuInventory.setTitle(_translate("MainWindow_home", "Inventory"))
        self.menuRegions.setTitle(_translate("MainWindow_home", "Regions"))
        self.menuStations.setTitle(_translate("MainWindow_home", "SubRegion"))
        self.menuSubStation.setTitle(_translate("MainWindow_home", "SubStation"))
        self.actionExit.setText(_translate("MainWindow_home", "Exit"))
        self.actionEquipments.setText(_translate("MainWindow_home", "Equipments"))


    def stock_entry(self):
        self.StockEntry = QtWidgets.QWidget()
        self.ui = Ui_StockEntry()
        self.ui.setupUi(self.StockEntry)
        self.StockEntry.show()

    def stock_detail(self):
        self.StockDetails = QtWidgets.QWidget()
        self.ui = Ui_StockDetails()
        self.ui.setupUi(self.StockDetails)
        self.StockDetails.show()

    def equipments(self):
        QMessageBox.information(self, "Equipments", "No Item or Fields yet for Equipments, still under construction")


    def report(self):
        QMessageBox.information(self, "Report", "No Report or Fields Generated for Stock, still under construction")




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow_home = QtWidgets.QMainWindow()
    ui = Ui_MainWindow_home()
    ui.setupUi(MainWindow_home)
    MainWindow_home.show()
    sys.exit(app.exec_())

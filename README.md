# Inventory App with Python3 using PyQt5 and QtDesigner

![pyqt_designer](screenshots/login_qtDesigner.png?raw=true "login")

![pyqt_designer](screenshots/home_qtDesigner.png?raw=true "home")

# About
This is a very basic inentory application done with python3 and pyqt5 
and qtDesigner. 

this project is not complete but enough to introduce the pyqt5 and 
qt_designer.

# Getting Started
1. install python:


	https://www.python.org/



2. set up a vitual environment:


	https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/


3. installing packages from requirements.txt
run the following command:

```
	pip install -r requirements.txt
```

4. installation of Mysql, go to:

	https://www.w3resource.com/mysql/mysql-installation-on-linux-and-windows.php

	https://dev.mysql.com/doc/workbench/en/wb-installing-windows.html

	https://dev.mysql.com/doc/workbench/en/wb-installing-linux.html

5. Creating a database table in phpMyadmin

https://www.liquidweb.com/kb/creating-tables-in-a-database-with-phpmyadmin/

# Setting Up and running Qt Designer on windows and linux
1. on windows do

```
	pip install pyqt5-tools
```

Go to C:\Program Files (x86)\Python36-32\Lib\site-packages\pyqt5-tools and locate designer.exe. Double click to open the Qt Designer.

2. on linux go to your application center locate Qt Designer and install

on your application menu, locate Qt designer Icon and dubble click to open


# Run application
from you terminal run 

```
	python Main.py
```

# Technologies
1. python2.x or 3.x
2. pyqt5
3. qtDesigner
4. Mysql and phpMyadmin

### Additional useful tools
1. pyinstall 	#to create an executable file for both linux and windows



# suggestions
you could fork the project and improve on alot of things in it and find out how to make an executable file for both windows and linux. mean while this apllication can also run on mac OS.

# Resources
1. https://www.python.org/
2. https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/
3. https://www.w3resource.com/mysql/mysql-installation-on-linux-and-windows.php
4. https://dev.mysql.com/doc/workbench/en/wb-installing-linux.html
5. https://dev.mysql.com/doc/workbench/en/wb-installing-windows.html
6. https://www.liquidweb.com/kb/creating-tables-in-a-database-with-phpmyadmin/
7. https://www.codementor.io/deepaksingh04/design-simple-dialog-using-pyqt5-designer-tool-ajskrd09n
8. https://pypi.org/project/pyqt5-tools/

### other Resources
1. https://build-system.fman.io/pyqt5-tutorial
2. https://www.riverbankcomputing.com/static/Docs/PyQt5/designer.html

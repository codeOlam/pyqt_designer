# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'StockEntry.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QApplication
import MySQLdb as mdb


class Ui_StockEntry(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()


    def setupUi(self, StockEntry):
        StockEntry.setObjectName("StockEntry")
        StockEntry.resize(951, 678)
        StockEntry.setMinimumSize(QtCore.QSize(951, 678))
        StockEntry.setMaximumSize(QtCore.QSize(951, 678))

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/powerIcon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        StockEntry.setWindowIcon(icon)

        self.groupBox = QtWidgets.QGroupBox(StockEntry)
        self.groupBox.setGeometry(QtCore.QRect(30, 50, 881, 291))
        font = QtGui.QFont()
        font.setFamily("Comfortaa")
        font.setPointSize(30)
        font.setBold(True)
        font.setWeight(75)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")

        self.label_ProductName = QtWidgets.QLabel(self.groupBox)
        self.label_ProductName.setGeometry(QtCore.QRect(10, 60, 100, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_ProductName.setFont(font)
        self.label_ProductName.setObjectName("label_ProductName")

        self.label_Date = QtWidgets.QLabel(self.groupBox)
        self.label_Date.setGeometry(QtCore.QRect(10, 110, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_Date.setFont(font)
        self.label_Date.setObjectName("label_Date")

        self.label_manufacturer = QtWidgets.QLabel(self.groupBox)
        self.label_manufacturer.setGeometry(QtCore.QRect(650, 60, 90, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_manufacturer.setFont(font)
        self.label_manufacturer.setObjectName("label_manufacturer")

        self.label_BatchNo = QtWidgets.QLabel(self.groupBox)
        self.label_BatchNo.setGeometry(QtCore.QRect(250, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_BatchNo.setFont(font)
        self.label_BatchNo.setObjectName("label_BatchNo")

        self.label_qty = QtWidgets.QLabel(self.groupBox)
        self.label_qty.setGeometry(QtCore.QRect(450, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_qty.setFont(font)
        self.label_qty.setObjectName("label_qty")

        self.lineEdit_productName = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_productName.setGeometry(QtCore.QRect(120, 60, 120, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_productName.setFont(font)
        self.lineEdit_productName.setObjectName("lineEdit_productName")

        self.lineEdit_BatchNo = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_BatchNo.setGeometry(QtCore.QRect(320, 60, 120, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_BatchNo.setFont(font)
        self.lineEdit_BatchNo.setObjectName("lineEdit_BatchNo")

        self.lineEdit_manufacturer = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_manufacturer.setGeometry(QtCore.QRect(750, 60, 120, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_manufacturer.setFont(font)
        self.lineEdit_manufacturer.setObjectName("lineEdit_manufacturer")

        self.lineEdit_qty = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_qty.setGeometry(QtCore.QRect(520, 60, 120, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_qty.setFont(font)
        self.lineEdit_qty.setObjectName("lineEdit_qty")

        self.dateEdit = QtWidgets.QDateEdit(self.groupBox)
        self.dateEdit.setGeometry(QtCore.QRect(80, 110, 131, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.dateEdit.setFont(font)
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setObjectName("dateEdit")

        self.groupBox_2 = QtWidgets.QGroupBox(self.groupBox)
        self.groupBox_2.setGeometry(QtCore.QRect(0, 160, 881, 131))
        self.groupBox_2.setMinimumSize(QtCore.QSize(881, 131))
        self.groupBox_2.setObjectName("groupBox_2")

        self.label_thickness = QtWidgets.QLabel(self.groupBox_2)
        self.label_thickness.setGeometry(QtCore.QRect(10, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_thickness.setFont(font)
        self.label_thickness.setObjectName("label_thickness")

        self.label_length = QtWidgets.QLabel(self.groupBox_2)
        self.label_length.setGeometry(QtCore.QRect(210, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_length.setFont(font)
        self.label_length.setObjectName("label_length")

        self.label_vector = QtWidgets.QLabel(self.groupBox_2)
        self.label_vector.setGeometry(QtCore.QRect(660, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_vector.setFont(font)
        self.label_vector.setObjectName("label_vector")

        self.label_weight = QtWidgets.QLabel(self.groupBox_2)
        self.label_weight.setGeometry(QtCore.QRect(430, 60, 63, 20))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.label_weight.setFont(font)
        self.label_weight.setObjectName("label_weight")

        self.lineEdit_thichness = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_thichness.setGeometry(QtCore.QRect(80, 60, 113, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_thichness.setFont(font)
        self.lineEdit_thichness.setObjectName("lineEdit_thichness")

        self.lineEdit_vector = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_vector.setGeometry(QtCore.QRect(740, 60, 113, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_vector.setFont(font)
        self.lineEdit_vector.setObjectName("lineEdit_vector")

        self.lineEdit_weight = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_weight.setGeometry(QtCore.QRect(520, 60, 113, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_weight.setFont(font)
        self.lineEdit_weight.setObjectName("lineEdit_weight")

        self.lineEdit_length = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_length.setGeometry(QtCore.QRect(290, 60, 113, 36))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(11)
        self.lineEdit_length.setFont(font)
        self.lineEdit_length.setObjectName("lineEdit_length")

        self.tabWidget = QtWidgets.QTabWidget(StockEntry)
        self.tabWidget.setGeometry(QtCore.QRect(40, 360, 861, 261))
        font = QtGui.QFont()
        font.setFamily("Comfortaa")
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_currentEntry = QtWidgets.QWidget()

        #Current tab
        self.tab_currentEntry.setObjectName("tab_currentEntry")
        self.tableWidget_currentEntry = QtWidgets.QTableWidget(self.tab_currentEntry)
        self.tableWidget_currentEntry.setGeometry(QtCore.QRect(0, 0, 861, 211))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.tableWidget_currentEntry.setFont(font)
        self.tableWidget_currentEntry.setObjectName("tableWidget_currentEntry")
        self.tableWidget_currentEntry.setColumnCount(0)
        self.tableWidget_currentEntry.setRowCount(0)
        self.tabWidget.addTab(self.tab_currentEntry, "")

        #Existing tab
        self.tab_Existing = QtWidgets.QWidget()
        self.tab_Existing.setObjectName("tab_Existing")
        self.tableWidget_Existing = QtWidgets.QTableWidget(self.tab_Existing)
        self.tableWidget_Existing.setGeometry(QtCore.QRect(0, 0, 861, 211))
        font = QtGui.QFont()
        font.setFamily("Cantarell")
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        self.tableWidget_Existing.setFont(font)
        self.tableWidget_Existing.setObjectName("tableWidget_Existing")
        self.tableWidget_Existing.setColumnCount(0)
        self.tableWidget_Existing.setRowCount(0)
        self.tabWidget.addTab(self.tab_Existing, "")


        self.pushButton_add = QtWidgets.QPushButton(StockEntry)
        self.pushButton_add.setGeometry(QtCore.QRect(680, 630, 92, 36))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_add.setIcon(icon1)
        self.pushButton_add.setObjectName("pushButton_add")

        self.pushButton_cancel = QtWidgets.QPushButton(StockEntry)
        self.pushButton_cancel.setGeometry(QtCore.QRect(790, 630, 92, 36))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_cancel.setIcon(icon2)
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.pushButton_cancel.clicked.connect(self.closeWin)

        self.retranslateUi(StockEntry)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(StockEntry)

    def retranslateUi(self, StockEntry):
        _translate = QtCore.QCoreApplication.translate
        StockEntry.setWindowTitle(_translate("StockEntry", "Stock Entry"))
        self.groupBox.setTitle(_translate("StockEntry", "Enter New Product"))
        self.label_ProductName.setText(_translate("StockEntry", "Product Name"))
        self.label_Date.setText(_translate("StockEntry", "Date"))
        self.label_manufacturer.setText(_translate("StockEntry", "Manufacturer"))
        self.label_BatchNo.setText(_translate("StockEntry", "Batch No"))
        self.label_qty.setText(_translate("StockEntry", "Quantity"))
        self.dateEdit.setDisplayFormat(_translate("StockEntry", "d/M/yy"))
        self.groupBox_2.setTitle(_translate("StockEntry", "Product Attributes"))
        self.label_thickness.setText(_translate("StockEntry", "Thickness"))
        self.label_length.setText(_translate("StockEntry", "Length"))
        self.label_vector.setText(_translate("StockEntry", "Vector"))
        self.label_weight.setText(_translate("StockEntry", "Weight"))

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_currentEntry), _translate("StockEntry", "Current Entry"))
 
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_Existing), _translate("StockEntry", "View Existing"))
        
        self.pushButton_add.setText(_translate("StockEntry", "Add"))
        self.pushButton_add.clicked.connect(self.insertData)

        self.pushButton_cancel.setText(_translate("StockEntry", "Cancel"))
        


    def insertData(self):
        con = mdb.connect('localhost', 'your_db_name', 'your_db_password', 'your_db_table')

        cur = con.cursor()

        cur.execute("INSERT INTO stock(product_name, batch_no, quantity, manufacturer, date_, thickness, length, vector)"
                        "VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (''.join(self.lineEdit_productName.text()),
                                                ''.join(self.lineEdit_BatchNo.text()),
                                                ''.join(self.lineEdit_qty.text()),
                                                ''.join(self.lineEdit_manufacturer.text()),
                                                ''.join(self.dateEdit.text()),
                                                ''.join(self.lineEdit_thichness.text()),
                                                ''.join(self.lineEdit_weight.text()),
                                                ''.join(self.lineEdit_vector.text())
                                                ))
        QMessageBox.about(self, 'db_insertData', 'Data inserted Successfully')
        self.close()


    def closeWin(self):
        pass






if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    StockEntry = QtWidgets.QWidget()
    ui = Ui_StockEntry()
    ui.setupUi(StockEntry)
    StockEntry.show()
    sys.exit(app.exec_())
